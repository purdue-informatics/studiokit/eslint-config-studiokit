# eslint-config-studiokit

> Shared config for eslint.

## Installation

If extending `studiokit`:

```sh
yarn add --dev eslint-config-studiokit @typescript-eslint/eslint-plugin @typescript-eslint/parser babel-eslint eslint eslint-config-prettier eslint-plugin-import eslint-plugin-prettier prettier
```

If extending `studiokit/react` (adds `eslint-config-react-app` and all peer dependencies):

```sh
yarn add --dev eslint-config-studiokit @typescript-eslint/eslint-plugin @typescript-eslint/parser babel-eslint eslint eslint-config-prettier eslint-plugin-import eslint-plugin-prettier prettier eslint-config-react-app@^5.2.0 eslint-plugin-flowtype@^3.1.2 eslint-plugin-jsx-a11y@^6.2.3 eslint-plugin-react@^7.18.3 eslint-plugin-react-hooks@^1.7.0
```

## Usage

Set your `eslintConfig` to:

```js
"eslintConfig": {
  "extends": [
    "studiokit"
  ]
}
```

Or for React support:

```js
"eslintConfig": {
  "extends": [
    "studiokit/react"
  ]
}
```

While Typescript rules are included, you may need to add the following to use your target project’s `tsconfig.json` and correct parser:

```js
"eslintConfig": {
  ...,
  "overrides": [
    {
      "files": [
        "**/*.ts?(x)"
      ],
      "parser": "@typescript-eslint/parser",
      "parserOptions": {
        "project": "tsconfig.json"
      }
    }
  ]
}
```

## [License](LICENSE)
