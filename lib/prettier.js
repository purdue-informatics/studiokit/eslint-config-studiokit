module.exports = {
	rules: {
		'prettier/prettier': [
			'error',
			{
				trailingComma: 'none',
				useTabs: true,
				tabWidth: 4,
				semi: false,
				singleQuote: true,
				printWidth: 120,
				jsxBracketSameLine: true,
				arrowParens: 'avoid',
				endOfLine: 'auto'
			}
		]
	}
}
