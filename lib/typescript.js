module.exports = {
	parser: '@typescript-eslint/parser',
	parserOptions: {
		sourceType: 'module'
		// project: 'tsconfig.json', // set this in your project overrides
	},
	env: {
		es6: true,
		node: true,
		browser: true,
		jest: true
	},
	extends: [
		'eslint:recommended',
		'plugin:@typescript-eslint/eslint-recommended',
		'plugin:@typescript-eslint/recommended',
		'plugin:@typescript-eslint/recommended-requiring-type-checking',
		'./order.js',
		'prettier',
		'prettier/@typescript-eslint',
		'./prettier.js'
	],
	plugins: ['@typescript-eslint', 'import', 'prettier'],
	rules: {
		'@typescript-eslint/explicit-function-return-type': 'off',
		'@typescript-eslint/explicit-module-boundary-types': 'off',
		'@typescript-eslint/interface-name-prefix': 'off',
		'@typescript-eslint/member-delimiter-style': [
			'off',
			{
				multiline: {
					delimiter: 'none',
					requireLast: true
				},
				singleline: {
					delimiter: 'semi',
					requireLast: false
				}
			}
		],
		'@typescript-eslint/no-empty-interface': 'off',
		'@typescript-eslint/no-explicit-any': 'off',
		'@typescript-eslint/no-unsafe-assignment': 'off',
		'@typescript-eslint/no-unsafe-return': 'off',
		'@typescript-eslint/no-unsafe-member-access': 'off',
		'@typescript-eslint/no-unsafe-call': 'off',
		'@typescript-eslint/restrict-template-expressions': 'off',

		'@typescript-eslint/ban-ts-comment': [
			'error',
			{
				'ts-expect-error': 'allow-with-description',
				'ts-ignore': 'allow-with-description',
				'ts-nocheck': 'allow-with-description',
				'ts-check': false,
				minimumDescriptionLength: 3
			}
		],

		'@typescript-eslint/ban-types': [
			'error',
			{
				types: {
					'{}': false
				}
			}
		],

		'no-use-before-define': 'off',
		'@typescript-eslint/no-use-before-define': [
			'warn',
			{
				functions: false,
				classes: false,
				variables: false,
				typedefs: false
			}
		],

		semi: 'off',
		'@typescript-eslint/semi': ['off', null],

		camelcase: 'off',
		'@typescript-eslint/camelcase': 'off'
	}
}
