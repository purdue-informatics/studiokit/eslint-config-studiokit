module.exports = {
	extends: ['react-app', './index.js', 'prettier/react', './lib/prettier.js'],
	overrides: [
		{
			files: ['**/*.ts?(x)'],
			extends: ['react-app', './lib/typescript.js', 'prettier/react', './lib/prettier.js']
		}
	]
}
